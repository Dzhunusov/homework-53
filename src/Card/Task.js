import React from 'react';

const Task = props => {
  return (
      <div className="task">
        <p className="task-item">#{props.id} {props.task}</p>
        <button className="task-del-btn" onClick={props.remove}>Delete</button>
      </div>
  );
};

export default Task;