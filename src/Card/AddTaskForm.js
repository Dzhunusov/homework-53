import React from 'react';

const AddTask = props => {
  return (
      <div className="task-add-form">
        <p><input className="task-text-entered" type="text" onChange={props.onChangeTask}/></p>
        <button className="task-add-btn" onClick={props.addNewTask}>Add Task</button>
      </div>
  );
};

export default AddTask;