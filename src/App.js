import React, {useState} from 'react';
import './App.css';
import Task from "./Card/Task";
import AddTask from "./Card/AddTaskForm";

const App = () => {
  const [tasks, setTasks] = useState([
    {task: 'Playing pool', id: 1},
    {task: 'Paint pic', id: 2},
    {task: 'Go to shop', id: 3},
    {task: 'Go to shop', id: 4},
  ]);

  const changeTask = (event, index) => {
    const  tasksCopy = [...tasks];
    const  taskCopy = {...tasks[index]};
    taskCopy.task = event.target.value;
    tasksCopy[index] = taskCopy;
    setTasks(tasksCopy);
  };

  const currentTask = (event, index) => {

  }

  const removeTask = index => {
    const tasksCopy = [...tasks];
    tasksCopy.splice(index, 1);
    setTasks(tasksCopy);
  }

  const taskList = tasks.map((task, index) => {
    return (
        <Task
          key={index}
          task={tasks[index].task}
          id={tasks[index].id}
          remove={() => removeTask(index)}/>
    )
  })

  return (
    <div className="App">
      <AddTask
        onChangeTask={event => changeTask(event, 0)}
        addNewTask={() => currentTask}/>
      {taskList}
    </div>
  );
}

export default App;
